import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';
import { FooterComponent } from './footer/footer.component';
import { SideNavComponent } from './side-nav/side-nav.component';
import { HeaderComponent } from './header/header.component';



@NgModule({
  declarations: [
    FooterComponent,
    SideNavComponent,
    HeaderComponent,
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports : [
    FooterComponent,
    SideNavComponent,
    HeaderComponent
  ]
})
export class LayoutModule { }
