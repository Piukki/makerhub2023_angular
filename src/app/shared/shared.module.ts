import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {PanelMenuModule} from 'primeng/panelmenu';
import {ButtonModule} from 'primeng/button';
import {MenubarModule} from 'primeng/menubar';


const importExportModules = [
  PanelMenuModule,
  ButtonModule,
  MenubarModule
]

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ...importExportModules
  ],
  exports : [
    ...importExportModules
  ]
})
export class SharedModule { }
