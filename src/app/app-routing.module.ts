import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{ path: 'user', loadChildren: () => import('./features/user/user.module').then(m => m.UserModule) }, { path: 'authentification', loadChildren: () => import('./features/authentification/authentification.module').then(m => m.AuthentificationModule) }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
